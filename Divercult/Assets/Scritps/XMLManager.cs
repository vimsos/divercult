﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class XMLManager : MonoBehaviour {

    #region SINGLETON
    public static XMLManager Instance;

    private void Awake()
    {
        if (Instance != this)
            Destroy(Instance);
        if (Instance == null)
            Instance = this;
    }
    #endregion

    public EntrevistadosDB entrevistadosDB;

    public void AdicionarEntrevistado (EntrevistadoEntry novoEntrevistado)
    {
        entrevistadosDB.EntrevistadoList.Add(novoEntrevistado);
    }

    //Save Function
    public void Save ()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(EntrevistadosDB));
        FileStream stream = new FileStream(Application.dataPath + "/StreamingAssets/XML/entrevistados_data.xml", FileMode.Create);
        serializer.Serialize(stream, entrevistadosDB);
        stream.Close();
    }

    //Load Function
    public void Load ()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(EntrevistadosDB));
        FileStream stream = new FileStream(Application.dataPath + "/StreamingAssets/XML/entrevistados_data.xml", FileMode.Open);
        entrevistadosDB = serializer.Deserialize(stream) as EntrevistadosDB;
        stream.Close();
    }
}

[System.Serializable]
public class EntrevistadoEntry {
    public string passaporte;
    public string curriculo;
    public string reportagem;
    public string paisDeOrigem;
    public string vagasDisponiveis;
    public bool aprovacao;

    public EntrevistadoEntry ()
    {

    }

    public EntrevistadoEntry(string _passaporte, string _curriculo, string _reportagem, string _paisDeOrigem, string _vagasDisponiveis, bool _aprovacao)
    {
        this.passaporte = _passaporte;
        this.curriculo = _curriculo;
        this.reportagem = _reportagem;
        this.paisDeOrigem = _paisDeOrigem;
        this.vagasDisponiveis = _vagasDisponiveis;
        this.aprovacao = _aprovacao;
    }
}
[System.Serializable]
public class EntrevistadosDB
{
    public List<EntrevistadoEntry> EntrevistadoList = new List<EntrevistadoEntry>();
}


