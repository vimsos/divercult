using UnityEngine;
using UnityEngine.UI;

namespace Divercult.Level1
{
	public class CardData : MonoBehaviour
	{
		[SerializeField] private Image image;
		[SerializeField] private Text description;
		[SerializeField] private Text imageMock;

		public int descriptionId { get; private set; }
		public int imageId { get; private set; }

		public void Setup(DeckCard deckCard, Sprite deckCardImage)
		{
			descriptionId = deckCard.descriptionId;
			imageId = deckCard.imageId;
			description.text = deckCard.descriptionId.ToString() + " " + deckCard.description;
			imageMock.text = deckCard.imageId.ToString();
			image.sprite = deckCardImage;
		}
	}
}