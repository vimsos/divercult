using UnityEngine;

namespace Divercult.Level1
{
	[RequireComponent(typeof(CardController))]
	public class CardBoardLocked : MonoBehaviour
	{
		private CardController card = null;

		private void Awake()
		{
			card = GetComponent<CardController>();
		}
		private void Start()
		{
			//manter este método mesmo que vazio
		}

		private void OnEnable()
		{
			Player.RemoveFromHand(card);
			card.transform.SetParent(Board.instance.BoardParent);
		}
	}
}