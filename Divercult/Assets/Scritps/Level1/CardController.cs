using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

namespace Divercult.Level1
{
	public enum CardState { OnStack, OnHand, BeingDragged, BoardLocked }

	public class CardController : MonoBehaviour
	{
		public const float MOVETRANSITIONDURATION = 0.225F;
		public const float FLIPTRANSITIONDURATION = 0.125F;
		public const float SCALETRANSITIONDURATION = 0.0625F;
		public const float ONMOUSEOVERSCALEFACTOR = 1.45F;
		public const float LOCALYPOSITION = -280F;
		public const float MAXLOCALXPOSITION = 500F;
		public const float CARDLOCKXOFFSET = 2.2875f;

		public static readonly Vector3 STACKPOSITION = new Vector3(-540f, 280f, 0f);

		public CardState State { get; private set; }
		public CanvasGroup BackSide { get { return backSideCanvasGroup; } }
		public CardCollider ImageSideCollider { get { return imageSideCollider; } }
		public CardCollider DescriptionSideCollider { get { return descriptionSideCollider; } }
		public ContactFilter2D Filter { get { return filter; } }

		[SerializeField] private CanvasGroup backSideCanvasGroup;
		[SerializeField] private CardCollider imageSideCollider;
		[SerializeField] private CardCollider descriptionSideCollider;
		[SerializeField] private ContactFilter2D filter;

		private List<MonoBehaviour> avaiableStates;

		private void Awake()
		{
			State = CardState.OnStack;

			avaiableStates = new List<MonoBehaviour>();

			avaiableStates.AddRange(new MonoBehaviour[] { GetComponent<CardOnStack>(), GetComponent<CardOnHand>(), GetComponent<CardBeingDragged>(), GetComponent<CardBoardLocked>() });

			TransitionTo(State);
		}

		public void TransitionTo(CardState newState)
		{
			avaiableStates[(int)State].enabled = false;
			State = newState;
			avaiableStates[(int)State].enabled = true;
		}
	}
}