using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Divercult.Level3
{
	public class HidableUI : MonoBehaviour
	{
		[Serializable] private enum UIState { Hidden, Shown };

		[SerializeField] private UIState state = UIState.Hidden;
		[SerializeField] private float transitionDuration = 0.1875f;

		private Vector3 hiddenPosition;
		private Vector3 shownPosition;

		private new RectTransform transform;

		private void Awake()
		{
			transform = gameObject.transform as RectTransform;
			hiddenPosition = transform.anchoredPosition;
			shownPosition = Vector3.zero;
		}

		public void Hide()
		{
			if (state == UIState.Hidden)
			{ return; }

			state = UIState.Hidden;

			transform.DOAnchorPos(hiddenPosition, transitionDuration);
		}

		public void Show()
		{
			if (state == UIState.Shown)
			{ return; }

			state = UIState.Shown;

			transform.DOAnchorPos(shownPosition, transitionDuration);
		}
	}
}